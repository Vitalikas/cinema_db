package cinema;

import cinema.entity.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import cinema.util.HibernateUtil;

import java.time.LocalDate;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Client cl1 = new Client("Name1", "Surname1", "name1@surname1.lt", LocalDate.now());
            Client cl2 = new Client("Name2", "Surname2", "name2@surname2.lt", LocalDate.now());
            Client cl3 = new Client("Name3", "Surname3", "name3@surname3.lt", LocalDate.now());

            Schedule sch1 = new Schedule(LocalDate.now().plusDays(1));
            Schedule sch2 = new Schedule(LocalDate.now().plusDays(2));
            Schedule sch3 = new Schedule(LocalDate.now().plusDays(3));
            Schedule sch4 = new Schedule(LocalDate.now().plusDays(4));
            Schedule sch5 = new Schedule(LocalDate.now().plusDays(5));

            Movie mov1 = new Movie("Titanic", "drama", 180, "ttt");
            Movie mov2 = new Movie("Matrix", "cyberpunk", 136, "mmm");
            Movie mov3 = new Movie("Interstellar", "science", 169, "iii");

            Room room1 = new Room(1, 200, "1st floor");
            Room room2 = new Room(2, 420, "2nd floor");
            Room room3 = new Room(3, 130, "3rd floor");

//            Set<Schedule> scheduleSet1 = new HashSet<>();
//            Collections.addAll(scheduleSet1, sch1, sch2, sch3);
//            mov1.setSchedules(scheduleSet1);
            Set<Movie> movieSet1 = new HashSet<>();
            Collections.addAll(movieSet1, mov1, mov3);
            sch1.setMovies(movieSet1);

//            Set<Schedule> scheduleSet2 = new HashSet<>();
//            Collections.addAll(scheduleSet2, sch2, sch4);
//            mov2.setSchedules(scheduleSet2);
            Set<Movie> movieSet2 = new HashSet<>();
            Collections.addAll(movieSet2, mov1, mov2);
            sch2.setMovies(movieSet2);

//            Set<Schedule> scheduleSet3 = new HashSet<>();
//            Collections.addAll(scheduleSet3, sch1, sch5);
//            mov3.setSchedules(scheduleSet3);
            Set<Movie> movieSet3 = new HashSet<>();
            Collections.addAll(movieSet3, mov1);
            sch3.setMovies(movieSet3);

            Set<Movie> movieSet4 = new HashSet<>();
            Collections.addAll(movieSet4, mov2);
            sch4.setMovies(movieSet4);

            Set<Movie> movieSet5 = new HashSet<>();
            Collections.addAll(movieSet5, mov3);
            sch5.setMovies(movieSet5);


//            saveAll(session, Arrays.asList(mov1, mov2, mov3));
            saveAll(session, Arrays.asList(sch1, sch2, sch3, sch4, sch5));
//            saveAll(session, Arrays.asList(room1, room2, room3));

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                e.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        // shutting down
        HibernateUtil.shutdown();
    }

    private static <T> void saveAll(Session session, Iterable<T> entities) {
        entities.forEach(session::saveOrUpdate);
    }
}
