package cinema.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "rooms")
@Getter
@Setter
@NoArgsConstructor
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11)
    private Long roomId;
    @Column(length = 11)
    private int number;
    @Column(length = 11)
    private int maxSeats;
    @Column(length = 45)
    private String location;

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Schedule> schedules = new ArrayList<>();
    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Seat> seats;

    public Room(int number, int maxSeats, String location) {
        this.number = number;
        this.maxSeats = maxSeats;
        this.location = location;
    }

    public void addSchedule(Schedule schedule) {
        schedules.add(schedule);
        schedule.setRoom(this);
    }
}
