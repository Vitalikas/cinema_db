package cinema.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "movies")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11)
    private Long movieId;
    @Column(length = 45)
    private String name;
    @Column(length = 45)
    private String category;
    @Column(length = 11)
    private int durationInMinutes;
    private String description;

    @ManyToMany(mappedBy = "movies", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Schedule> schedules;

    public Movie(String name, String category, int durationInMinutes, String description) {
        this.name = name;
        this.category = category;
        this.durationInMinutes = durationInMinutes;
        this.description = description;
    }

//    public void addSchedule(Schedule schedule) {
//        schedules.add(schedule);
//        schedule.setMovie(this);
//    }

//    public void addSchedule(Schedule schedule) {
//        if (schedules == null) {
//            schedules = new ArrayList<>();
//        }
//        schedules.add(schedule);
//    }
}
