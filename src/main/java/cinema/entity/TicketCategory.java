package cinema.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ticketCategories")
@Getter
@Setter
@NoArgsConstructor
public class TicketCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 11)
    private Long ticketCategoryId;
    @Column(length = 45)
    private String type;
    @Column(length = 11)
    private int price;

    @OneToMany(mappedBy = "ticketCategory", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Ticket> tickets;

    public TicketCategory(String type, int price) {
        this.type = type;
        this.price = price;
    }
}
